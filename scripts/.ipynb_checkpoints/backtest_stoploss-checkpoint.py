import numpy as np
import pandas as pd
from scipy import stats
from dateutil import parser
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import statistics
from collections import defaultdict
import json
import datetime
import time

DATA_PATH = 'processed_data/'

all_data = pd.read_csv(DATA_PATH + 'allDataCombined.csv')
price_data = pd.read_csv(DATA_PATH + 'priceData.csv')
df = pd.merge(all_data, price_data, on="timestamp")
df['date'] = df['timestamp'].apply(lambda x: parser.parse(x).date())
df['time'] = df['timestamp'].apply(lambda x: parser.parse(x).strftime("%H:%M:%S"))
corr = []
corr_all = []
pnl = defaultdict(list)
index = 0

df['NOPE'] = df['NOPE_busVolume'] * 100

#for short testing, remove feb 19 2020 to march 23 2020
temp1 = df[df['date'] < datetime.date(2020, 2, 19)] 
temp2 = df[df['date'] > datetime.date(2020, 3, 23)] 


def backtest_short(day_group, short_entry, short_exit, stop, cutoff_time):
    values = []
    trade_in_progress = False
    entry_price = None
    exit_price = None
    total_pnl = 0
    for index, row in day_group.iterrows():
        if row['NOPE_busVolume']*100 >= short_entry and not trade_in_progress and row['time'] > '09:45:00' and row['time'] < 'cutoff_time':
            entry_price = (row['NOPE_busVolume']*100, row['time'], row['active_underlying_price'])
            trade_in_progress = True
        if trade_in_progress and (row['NOPE_busVolume']*100 <= short_exit or row['NOPE_busVolume']*100 >= stop):
            exit_price = (row['NOPE_busVolume']*100,row['time'],row['active_underlying_price'])
            values.append((entry_price, exit_price))
            total_pnl = total_pnl + (entry_price[2] - exit_price[2])
            trade_in_progress = False
            entry_price = None
            exit_price = None
        if row['time'] == '16:00:00':
            if trade_in_progress:
                exit_price = (row['NOPE_busVolume']*100,row['time'],row['active_underlying_price'])
                values.append((entry_price, exit_price))
                total_pnl = total_pnl + (entry_price[2] - exit_price[2])
                trade_in_progress = False
                entry_price = None
                exit_price = None
            break
    return (values, total_pnl)


#-----

# test stop loss

pnl_list = []
t0 = time.time()

pnl_list = np.zeros([int((200-20)/5)])

for k, stop_loss in enumerate(range(20, 200, 5)):
    total_pnl = defaultdict(tuple)
    real_total_pnl = 0
    for name, group in df.groupby('date'):
        total_pnl[str(name)] = backtest_short(group, 30, 15, stop_loss, '14:30:00')
        real_total_pnl = real_total_pnl + total_pnl[str(name)][1]
    pnl_list[k] = real_total_pnl

t1 = time.time()
t1 - t0

print(pnl_list)