import numpy as np
import pandas as pd
from scipy import stats
from dateutil import parser
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import statistics
from collections import defaultdict
import json
import datetime
import time
import datetime as dt

DATA_PATH = 'processed_data/'

all_data = pd.read_csv(DATA_PATH + 'allDataCombined.csv')
price_data = pd.read_csv(DATA_PATH + 'priceData.csv')
df = pd.merge(all_data, price_data, on="timestamp")
df['date'] = df['timestamp'].apply(lambda x: parser.parse(x).date())
df['time'] = df['timestamp'].apply(lambda x: parser.parse(x).strftime("%H:%M:%S"))
corr = []
corr_all = []
pnl = defaultdict(list)
index = 0

df['NOPE'] = df['NOPE_busVolume'] * 100

#for short testing, remove feb 19 2020 to march 23 2020
temp1 = df[df['date'] < datetime.date(2020, 2, 19)] 
temp2 = df[df['date'] > datetime.date(2020, 3, 23)] 
dfshort = pd.concat([temp1, temp2], axis=0)
dfshort = dfshort.reset_index(drop=True)
#add time column in datetime
dfshort['timeDT'] = pd.to_datetime(dfshort['time'])

def backtest_short(day_group, start_time, stop_time, short_entry):
    drawdown_values = []
    entry_price = 0
    exit_price = 0
    exit_index = 0
    max_drawdown = 0
    total_pnl = 0
    win_count = 0
    loss_count = 0
    nope_success = False
    
    i = 0
    rowlimit = len(day_group)
    #loop through rows in a day
    while i < rowlimit:
        #set time limit
        if day_group['time'][i] > stop_time:
            break
        if day_group['NOPE'][i] >= short_entry and day_group['time'][i] > start_time and day_group['time'][i] < stop_time:
            entry_price = day_group['active_underlying_price'][i]
            #check that there is a NOPE < 15 in the next two hours
            for j in range(1, 24):
                #if NOPE < 15, sell
                if i+j < 81 and day_group['NOPE'][i+j] < 15:
                    nope_success = True
                    exit_index = i+j
                    exit_price = day_group['active_underlying_price'][exit_index]
                    total_pnl = total_pnl + (entry_price - exit_price)
                    if exit_price < entry_price:
                        win_count = win_count + 1
                    else:
                        loss_count = loss_count + 1
                    break
            #increment counter
            i = i+j   
            #check if NOPE <15 tripped, if not, fail the trade
            if nope_success == False:
                exit_price = day_group['active_underlying_price'][exit_index]
                total_pnl = total_pnl + (entry_price - exit_price)
                loss_count = loss_count + 1
            else:
                nope_success = True
        else:
            i = i + 1
    return [total_pnl, win_count, loss_count]





days = []
for name, group in dfshort.groupby('date'):
    days.append(group)

for i in range(0, len(days)):
    days[i] = days[i].reset_index(drop=True)


#starttimes = ['09:45:00', '10:15:00', '10:45:00', '11:15:00', '11:45:00', '12:15:00', '12:45:00', '13:15:00', '13:45:00', '14:15:00']
#stoptimes =  ['10:15:00', '10:45:00', '11:15:00', '11:45:00', '12:15:00', '12:45:00', '13:15:00', '13:45:00', '14:15:00', '14:45:00']\
starttimes = ['10:15:00']
stoptimes = ['14:45:00']
wins = []
loss = []
ratio = []

t0 = time.time()
for j  in range(0, len(starttimes)):
    real_total_pnl = 0
    total_wins = 0
    total_loss = 0
    for i, elem in enumerate(days):
        temp = backtest_short(elem, starttimes[j], stoptimes[j], 30)
        real_total_pnl = real_total_pnl + temp[0]
        total_wins = total_wins + temp[1]
        total_loss = total_loss + temp[2]
    if(total_wins + total_loss) > 0:
        wins.append(total_wins)
        loss.append(total_loss)
        ratio.append(total_wins / (total_loss + total_wins) * 100)
        print("Start time: " + starttimes[j] + " " + str(total_wins) + ', ' + str(total_loss) + " " + str(ratio[j]), )
    
t1 = time.time()
print(ratio)
